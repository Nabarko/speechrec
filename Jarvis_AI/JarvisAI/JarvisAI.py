'''
Created on May 20, 2019

@author: eroynab
'''

from configparser import ConfigParser
import os
import smtplib, ssl
import webbrowser
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import pyttsx3, datetime
import speech_recognition as sr
import wikipedia as wiki
import PyPDF2 as pdf


engine = pyttsx3.init('sapi5')
voices = engine.getProperty('voices')
#print voices[0].id
engine.setProperty('voices', voices[0].id)
engine.setProperty('rate', 110)

def gmail_config():
    parser = ConfigParser()
    parser.read('AIconfig.cfg', encoding='utf-8')
    sender = str(parser.get('Gmail properties', 'gmail_sender'))
    reciever = parser.get('Gmail properties', 'gmail_reciever')
    password = str(parser.get('Gmail properties', 'gmail_sender_password'))
    return sender,reciever,password

def outlook_config():
    parser = ConfigParser()
    parser.read('AIconfig.cfg', encoding='utf-8')
    sender = str(parser.get('Ericsson Mail properties', 'outlook_sender'))
    reciever = parser.get('Ericsson Mail properties', 'outlook_reciever').split(",")
    return sender,reciever

def pdf_file_config():
    parser = ConfigParser()
    parser.read('AIconfig.cfg', encoding='utf-8')
    file_path = os.path.abspath(parser.get('Pdf Reader properties', 'filePath'))
    return file_path

def speak(audio):
    engine.say(audio)
    engine.runAndWait()

def wishme():
    hour = int(datetime.datetime.now().hour)
    if hour>=0 and hour<12:
        speak("Good Morning!")
    elif hour>=12 and hour<18:
        speak("Good Afternoon!")
    else:
        speak("Good Evening!")

def aboutme():
    speak("My Name is Jarvis and I am your personal AI assistant")
    speak("Please tell me how can I assist you")

def takecommand():
    """
    It takes microphone Input from the user and returns a text.
    """
    rec = sr.Recognizer()
    with sr.Microphone() as source:
        print("Listening..........")
        speak("Listening..........")
        rec.pause_threshold = 1
        audio = rec.listen(source,timeout=5, phrase_time_limit=5)
        rec.operation_timeout
    try:
        print ("Recognizing....")
        query = rec.recognize_google(audio, language='en-in')
        print("User said: {0}".format(query))
    except Exception as e:
        print(e)
        print("Please say that Again ..... ")
        return "None"
    return query

def Email_Subject():
    try:
        print("What is your Subject for Email to be written")
        speak("What is your Subject for Email to be written")
        subject = takecommand()
    except Exception as e:
        print("My recognition is not Up to date")
    return subject

def sendEmailviaGmail(sender,sender_password,reciever,content):
    """
    DocString: Sends email over Gmail with the given sender and reciever/reciever_list
    """
    msg = MIMEMultipart()
    subject = Email_Subject()
    msg['Subject'] = str(subject)
    msg.attach(MIMEText(content,'plain'))
    try:  
        server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        server.ehlo()
        server.login(sender, sender_password)
        server.sendmail(sender, reciever, msg.as_string())
        server.close()
    except smtplib.SMTPException as e:  
        print ('Something went wrong...'+e)
        
def sendEmailviaOutlook(sender,reciever,content):
    """
    DocString: Sends email over ericsson corporate mail id with the given sender
               and reciever/reciever_list
    """
    msg = MIMEMultipart()
    subject = Email_Subject()
    msg['Subject'] = str(subject)
    msg.attach(MIMEText(content,'plain'))
    try:  
        server = smtplib.SMTP('smtp.internal.ericsson.com', 25)
        server.ehlo()
        server.sendmail(sender, reciever, msg.as_string())
        server.close()
    except smtplib.SMTPException as e:  
        print ('Something went wrong...'+e)

def pdfVoiceReader(file_path):
    pdfObj = open(file_path, 'rb')
    pdfReader = pdf.PdfFileReader(pdfObj)
    pdflength = pdfReader.getNumPages()
    for i in xrange(pdflength):
        pgObj = pdfReader.getPage(i)
        fileLine = pgObj.extractText()
        print str(fileLine)
        speak(fileLine.strip())
    
    
if __name__ == '__main__':
    #wishme()
    #aboutme()
    say = True
    while say:
        query = takecommand().lower()
        #logic for executing tasks
        if 'wikipedia' in query:
            speak('Searching Wikipedia.......')
            query = query.replace("wikipedia", "")
            results = wiki.summary(query, sentences=5)
            speak("According to Wikipedia")
            print(results)
            speak(results)
        elif 'youtube' in query:
            webbrowser.open("youtube.com")
        elif 'google' in query:
            webbrowser.open("google.com")
        elif 'stackoverflow' in query:
            webbrowser.open("stackoverflow.com")
        elif 'play music' in query:
            music_dir = raw_input("Enter your favorite directory for songs")
            songs = os.listdir(os.path.abspath(music_dir))
            os.startfile(os.path.join(music_dir, songs[0]))
        elif 'time' in query:
            strTime = datetime.datetime.now().strftime("%H:%M:%S")
            speak("The Time is "+strTime)
        elif 'gmail' in query:
            try:
                speak("What to write in the Email")
                content = takecommand()
                sender,to,password = gmail_config()
                sendEmailviaGmail(sender,password,to,content)
                speak("Email has been sent")
            except Exception as e:
                print e
                speak("Sorry. Cannot send the Email")
                print("Email is not sent. Please Check your configurations")
        elif (('ericsson' in query) or ('outlook' in query)):
            try:
                speak("What to write in the Email")
                content = takecommand()
                sender,to = outlook_config()
                sendEmailviaOutlook(sender,to,content)
                speak("Email has been sent")
            except Exception as e:
                print e
                speak("Sorry. Cannot send the Email")
                print("Email is not sent. Please Check your configurations")
        elif ('pdf' in query):
            try:
                file_path = pdf_file_config()
                file_name = os.path.split(file_path)[1]
                speak("Reading the contents of {0}".format(file_name))
                pdfVoiceReader(file_path)
            except Exception as e:
                print e
                speak("Problem in reading the PDF")
        elif 'exit' in query:
            say = False
        
            
    